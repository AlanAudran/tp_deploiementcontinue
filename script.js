function changeText() {
    document.querySelector('h1').textContent = "Text Changed!";
}

function showCurrentTime() {
    const now = new Date();
    const timeString = now.toLocaleTimeString();
    document.getElementById('currentTime').textContent = `Current Time: ${timeString}`;
}

// Ajoute un paragraphe dans le HTML pour afficher l'heure
window.onload = function() {
    const timeDisplay = document.createElement('p');
    timeDisplay.id = 'currentTime';
    document.getElementById('app').appendChild(timeDisplay);
    showCurrentTime();
    setInterval(showCurrentTime, 1000);
}
