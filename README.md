# TP_DéploiementContinue

Alan Audran / Valentin Bouet / Douglas Barlow 

## Documentation

### Préparation de l'environnement :
Le projet commence par la création d'un projet sur GitLab et l'initialisation d'un dépôt Git local dans Visual Studio Code . 

### Développement de l'application :
L'application est développée en HTML, CSS, et JavaScript. Elle inclut une page web affichant un message statique et l'heure actuelle qui se met à jour chaque seconde.

### Containerisation avec Docker :
Un Dockerfile est créé pour définir l'environnement de l'application, en utilisant nginx:alpine comme image de base pour servir les fichiers statiques. L'image Docker est construite et testée localement.

### Automatisation avec GitLab CI/CD :
Le fichier .gitlab-ci.yml est configuré pour automatiser les étapes de build et de déploiement. Le pipeline CI/CD inclut la construction de l'image Docker et son déploiement.

### Déploiement :
Les modifications sont poussées vers GitLab, déclenchant le pipeline CI/CD. L'exécution réussie du pipeline assure que l'application est construite et déployée correctement.

### Collaboration et améliorations :
Une fonctionnalité permettant d'afficher l'heure actuelle est ajoutée à l'application. Les fonctionnalités de merge requests de GitLab facilitent l'intégration des modifications.

### Documentation :
Cette documentation résume le flux de travail et les commandes utilisées pour le déploiement de l'application. Elle sert de référence pour des déploiements futurs.

### Conclusion :
L'utilisation de GitLab CI/CD et Docker simplifie le processus de déploiement, en rendant les mises à jour de l'application fluides et automatisées. Ce projet démontre l'efficacité d'un pipeline d'intégration et de déploiement continu.
